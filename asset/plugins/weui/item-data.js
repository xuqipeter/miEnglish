const item_data = {
  english: {
    title: '英语',
    headerImg:'mob'
  },
  japanese: {
    title: '日语',
    headerImg:'mry'
  },
  korean: {
    title: '韩语',
    headerImg:'mhy'
  },
  german: {
    title: '德语',
    headerImg:'mdy'
  },
  french: {
    title: '法语',
    headerImg:'mfy'
  },
  spanish: {
    title: '西班牙语',
    headerImg:'mxy'
  },
  russian: {
    title: '俄语',
    headerImg:'mey'
  },
  italian: {
    title: '意大利语',
    headerImg:'myy'
  },
};
export {item_data as default};