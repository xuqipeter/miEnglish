// pages/home/home.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    logoSrc: "http://www.ichimay.com/mob/images/logo.jpg",
    lessonArray: [{
        name: "英语",
        background: "http://120.27.231.145/WX_IMAGE/01.jpg?ver=20171213",
        href:"english"
      },
      {
        name: "日语",
        background: "http://120.27.231.145/WX_IMAGE/02.jpg?ver=20171213",
        href:"japanese"
      },
      {
        name: "韩语",
        background: "http://120.27.231.145/WX_IMAGE/03.jpg?ver=20171213",
        href:"korean"
      },
      {
        name: "德语",
        background: "http://120.27.231.145/WX_IMAGE/04.jpg?ver=20171213",
        href:"german"
      },
      {
        name: "法语",
        background: "http://120.27.231.145/WX_IMAGE/05.jpg?ver=20171213",
        href:"french"
      },
      {
        name: "西班牙语",
        background: "http://120.27.231.145/WX_IMAGE/06.jpg?ver=20171213",
        href:"spanish"
      },
      {
        name: "俄语",
        background: "http://120.27.231.145/WX_IMAGE/07.jpg?ver=20171213",
        href:"russian"
      },
      {
        name: "意大利语",
        background: "http://120.27.231.145/WX_IMAGE/08.jpg?ver=20171213",
        href:"italian"
      }
    ],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  /* 去内容页面 */
  gotoItem: function(event){
    let href=event.target.dataset.href;
    // console.log(event.target);
    let url='item/item?href='+href;
    wx.navigateTo({
      url: url
    })
  },
  /* 打电话 */
  calling:function(){
    wx.makePhoneCall({
      phoneNumber: '029-87656326',
      success: function(res) {
        // success
        console.log("拨打电话成功！")
      },
      fail:function(){
        console.log("拨打电话失败！")
      }
    })
  }
})