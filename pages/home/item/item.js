// pages/home/item/item.js
import bs_data from '../../../asset/plugins/weui/item-data'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    logoSrc: "http://www.ichimay.com/mob/images/logo.jpg",
    headerImg:null,
    language:null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var href= options.href;
    var itemData=bs_data[href];
    console.log(itemData);
    this.setData({
      headerImg:'http://www.ichimay.com/'+itemData.headerImg+'/images/banner.jpg',
      language:itemData.title,
    })
    wx.setNavigationBarTitle({
      title:'智美'+itemData.title,
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  /* 打电话 */
  calling:function(){
    wx.makePhoneCall({
      phoneNumber: '029-87656326',
      success: function(res) {
        // success
        console.log("拨打电话成功！")
      },
      fail:function(){
        console.log("拨打电话失败！")
      }
    })
  },
  goReturn:function(){
    wx.navigateBack();
  }
})